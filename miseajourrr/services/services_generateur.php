<?php

function code_aleatoire(&$donnees){
    $cpt=1;
    $code_aleatoire = rand(100000,999999);
    $fr = substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ"),0,2);
    $vrai_code = $fr.$code_aleatoire;
    while($cpt!=0){
        $cpt=0;
        foreach($donnees as $valeur){
            if ($vrai_code == $valeur){
                $code_aleatoire = rand(100000,999999);
                $fr = substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ"),0,2);
                $vrai_code = $fr.$code_aleatoire;
                $cpt=1;
            }
        }
        if($cpt==0){
            $donnees[]=$vrai_code;
            return $vrai_code;
        }
    }
}

function numero_aleatoire(&$donnees1){
        $cpt=1;
        $numero_aleatoire = rand(10000000000,99999999999);
        while($cpt!=0){
            $cpt=0;
            foreach($donnees1 as $valeur2){
                if ($numero_aleatoire == $valeur2){
                    $numero_aleatoire = rand(10000000000,99999999999);
                    $cpt=1;
                }
            }
            if($cpt==0){
                $donnees1[]=$numero_aleatoire;
            }
        }
        return $numero_aleatoire;
        
}

function code_agence(&$donnees3){
    $cpt=1;
        $numero_aleatoire = rand(100,999);
        while($cpt!=0){
            $cpt=0;
            foreach($donnees3 as $valeur2){
                if ($numero_aleatoire == $valeur2){
                    $numero_aleatoire = rand(100,999);
                    $cpt=1;
                }
            }
            if($cpt==0){
                $donnees3[]=$numero_aleatoire;
            }
        }
        return $numero_aleatoire;
}

if (($handle = fopen("./a_imprimer/infos_clients.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
           
           $client["nom"]= $data[0];
           $client["prenom"]=$data[1];
           $client["naissance"]= $data[2];
           $client["identifiant"]=$data[3];
           $client["mail"]= $data[4];
           $client["decouvert"]=$data[5];
           $clients[]=$client;

    }
    fclose($handle);
}

if (($handle = fopen("./a_imprimer/infos_agences.csv", "r")) !== FALSE) {
    while (($data2 = fgetcsv($handle, 1000, ";")) !== FALSE) {
           
           $agence["nom"]= $data2[0];
           $agence["code"]=$data2[1];
           $agence["adresse"]= $data2[2];
           $agence["postal"]=$data2[3];
           $agences[]=$agence;

    }
    fclose($handle);
}

if (($handle = fopen("./a_imprimer/infos_comptes.csv", "r")) !== FALSE) {
    while (($data1 = fgetcsv($handle, 1000, ";")) !== FALSE) {
           
           $compte["codeagence"]= $data1[0];
           $compte["adresseagence"]=$data1[1];
           $compte["nomagence"]= $data1[2];
           $compte["numero"]=$data1[3];
           $compte["identifiant"]= $data1[4];
           $compte["nom"]=$data1[5];
           $compte["prenom"]= $data1[6];
           $compte["naissance"]=$data1[7];
           $compte["mail"]= $data1[8];
           $compte["solde"]=$data1[9];
           $compte["frais"]= $data1[10];
           $compte["type"]=$data1[11];
           $compte["decouvert"]=$data1[12];
           $comptes[]=$compte;

    }
    fclose($handle);
}

?>